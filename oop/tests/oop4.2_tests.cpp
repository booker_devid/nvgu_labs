/*
    Тесты для проверки корректности работоспособности поиска длины
*/ 

#include "../oop4/oop4.hpp"
#include <algorithm>
#include <gtest/gtest.h>

// Проверка инициализации и базового функционала
TEST(TestOOP4_2, dynamic_array_empty) {
    // Проверка массива целых чисел
    const auto expect_length_int = 0;
    const auto expect_max_int = 0;
    const auto expect_min_int = 0;

    auto ArrayInt = dynamicArray<int>(&std::cout);
    ASSERT_EQ(expect_length_int, ArrayInt.Length());
    ASSERT_EQ(expect_max_int, ArrayInt.Max());
    ASSERT_EQ(expect_max_int, ArrayInt.Min());

    // Проверка массива дробных чисел
    const auto expect_length_double = 0;
    const auto expect_max_double = 0.0;
    const auto expect_min_double = 0.0;

    auto ArrayDouble = dynamicArray<double>(&std::cout);
    ASSERT_EQ(expect_length_int, ArrayDouble.Length());
    ASSERT_EQ(expect_max_double, ArrayDouble.Max());
    ASSERT_EQ(expect_max_double, ArrayDouble.Min());

    // Проверка массива символов
    const auto expect_length_char = 0;
    const auto expect_max_char = '0';
    const auto expect_min_char = '0';

    auto ArrayChar = dynamicArray<char>(&std::cout);
    ASSERT_EQ(expect_length_int, ArrayChar.Length());
    ASSERT_EQ(expect_max_char, ArrayChar.Max());
    ASSERT_EQ(expect_max_char, ArrayChar.Min());
}

// Проверка функционала для целых типов
TEST(TestOOP4_2, dynamic_array_int) {
    // Проверка массива целых чисел
    const auto expect_length_int = 5;
    const auto expect_max_int = 9;
    const auto expect_min_int = -6;

    const int test_element_1 = expect_max_int;
    const int test_element_2 = -4;
    const int test_element_3 = 3;
    const int test_element_4 = expect_min_int;
    const int test_element_5 = 1;

    const int expect_sort_int[] = {
        test_element_4, // -6
        test_element_2, // -4
        test_element_5, // 1
        test_element_3, // 3
        test_element_1, // 9
    };

    auto ArrayInt = dynamicArray<int>(&std::cout);

    ArrayInt.Add(test_element_1);
    ArrayInt.Add(test_element_2);
    ArrayInt.Add(test_element_3);
    ArrayInt.Add(test_element_4);
    ArrayInt.Add(test_element_5);

    ASSERT_EQ(expect_length_int, ArrayInt.Length());
    ASSERT_EQ(expect_max_int, ArrayInt.Max());
    ASSERT_EQ(expect_min_int, ArrayInt.Min());

    ArrayInt.Sort();
    for (int idx = 0; idx < 5; idx++) {
        ASSERT_EQ(expect_sort_int[idx], ArrayInt[idx]);
    }
}

// Проверка функционала для дробных типов
TEST(TestOOP4_2, dynamic_array_double) {
    // Проверка массива целых чисел
    const auto expect_length_double = 5.0;
    const auto expect_max_double = 9.1;
    const auto expect_min_double = -6.0;

    const double test_element_1 = expect_max_double;
    const double test_element_2 = -4.0;
    const double test_element_3 = 3.0;
    const double test_element_4 = expect_min_double;
    const double test_element_5 = 1.0;

    const double expect_sort_double[] = {
        test_element_4, // -6
        test_element_2, // -4
        test_element_5, // 1
        test_element_3, // 3
        test_element_1, // 9.1
    };

    auto ArrayDouble = dynamicArray<double>(&std::cout);

    ArrayDouble.Add(test_element_1);
    ArrayDouble.Add(test_element_2);
    ArrayDouble.Add(test_element_3);
    ArrayDouble.Add(test_element_4);
    ArrayDouble.Add(test_element_5);

    ASSERT_EQ(expect_length_double, ArrayDouble.Length());
    ASSERT_EQ(expect_max_double, ArrayDouble.Max());
    ASSERT_EQ(expect_min_double, ArrayDouble.Min());

    ArrayDouble.Sort();
    for (int idx = 0; idx < 5; idx++) {
        ASSERT_EQ(expect_sort_double[idx], ArrayDouble[idx]);
    }
}
