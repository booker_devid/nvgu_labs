/*
    Тесты для проверки корректности работоспособности поиска длины
*/ 

#include "../oop4/oop4.hpp"
#include <gtest/gtest.h>

// Проверка нахождение минимального значения
TEST(TestOOP4_1, search_min_sequence1) {
    int args[] = {4,8,2,3};
    const auto expect_value = 2;

    // Получем значение и округляем до двух знаков после точки
    const auto minValue = FindMin<int>(&args);
    ASSERT_EQ(expect_value, minValue);
}

TEST(TestOOP4_1, search_min_sequence2) {
    double args[] = {-4.0,-3.0,-2.0,-1.0};
    const auto expect_value = -4.0;

    // Получем значение и округляем до двух знаков после точки
    const auto minValue = FindMin<double>(&args);
}

TEST(TestOOP4_1, search_min_sequence3) {
    char args[] = {'9','3','5','6'};
    const auto expect_value = '3';

    // Получем значение и округляем до двух знаков после точки
    const auto minValue = FindMin<char>(&args);
    ASSERT_EQ(expect_value, minValue);
}

// Проверка отработки на пустой указатель
TEST(TestOOP4_1, search_min_empty_args) {
    const auto expect_value = 0;

    // Получем значение и округляем до двух знаков после точки
    const auto minValue = FindMin<int>(nullptr);
    ASSERT_EQ(expect_value, minValue);
}

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}