// Шаблонный класс для реализации динамического массива
#include <cstdint>
#include <cstring>
#include <ostream>

#include "./oop4.hpp"

// Стартовый размер массива
const int startCapacity = 8;
// Разделитель списка при выводе 
const char * showElementSeparator = ",";

// Конструктор
template <class T> dynamicArray<T>::dynamicArray(std::ostream *out)  {
    this->_outStream = out;
    this->cap = startCapacity;
    this->_length = 0;
    this->_idxMaxValue = 0;
    this->_idxMinValue = 0;
    this->_values = (T*) malloc(sizeof(T)*startCapacity);
}

// Деструктор
template <class T> dynamicArray<T>::~dynamicArray()  {
    free(_values);
}

// Функция конкурентно не безопасна!
// Добавление нового элемента.
template <class T> void dynamicArray<T>::Add(T newValue) {
    int newCapacity = 0;

    // Изначальный размер
    if (this->cap == 0) {
        newCapacity = startCapacity;
    }

    // Первый элемент не влияет на индекс максимального и минимального значения
    if (this->_length > 0) {
        // При добавлении элемента проверяет его на максимальное значение по сравнению с текущим
        if (newValue > this->_values[this->_idxMaxValue]) {
            this->_idxMaxValue = this->_length;
        } 
        
        // При добавлении элемента проверем его на минимальное значение по сравнению с текущим
        if (newValue < this->_values[this->_idxMinValue]) {
            this->_idxMinValue = this->_length;
        }
    }

    // Добавление нового значения в массив
    this->_values[this->_length] = newValue;
    this->_length++;
    
    // При увеличении количества элементов capacity (размерность) увеличивается в 2 раза.
    // Но при достижении больших размеров выделение памяти под следующее выделение сокращается.
    if (this->_length < this->cap) {
        return;
    }
    if (this->_length > 0) {
        newCapacity = this->_length*2;
    }
    if (this->_length > UINT8_MAX) {
        newCapacity = (int) this->_length*1.7;
    }
    if (this->_length > UINT16_MAX/2) {
        newCapacity = (int) this->_length*1.5;
    }
    if (this->_length > UINT16_MAX) {
        newCapacity = (int) this->_length*1.2;
    }

    // Аллокация новой памяти 
    T *buf = (T*) malloc(sizeof(T)*newCapacity);
    this->cap = newCapacity;
    
    // Копирование старого массива
    memcpy(buf, this->_values, sizeof(T)*this->_length);

    // Чистим устаревший массив
    free(this->_values);
    
    // Заполняем массив новым буфером выделенным под новый размер
    this->_values = buf;
}

// Не работает для кириллицы в UTF-8 из-за размерности символа 
// в 2 байта и нюансов расстановки символов ЁёЙй.
template <class T> void dynamicArray<T>::Sort() {
    T buf;
    bool ready; 

    // Сортировка будет проходить по массиву с начала 
    // до конца, до перехода ready в состояние true.
    while (!ready) {

        // При каждом начале считаем что список отсортирован
        ready = true;

        for (int idx=0; idx<(this->_length-1); idx++) {
            // Буфер хранит временно текущее значения
            buf = this->_values[idx];

            // В случае если текущее значение больше предыдущего 
            // требуется помнить их местами
            if (this->_values[idx] > this->_values[idx+1]) {
                this->_values[idx] = this->_values[idx+1];
                this->_values[idx+1] = buf;

                // В случае хотя бы одной перестановки помечаем ready - false.
                // Если замен не будет, можем считать список отсортированным
                ready=false;
            }
        }
    }

    // После сортировки минимальное и максимальное значение 
    // это первый и последний элемент массива
    this->_idxMinValue = 0;
    this->_idxMaxValue = 1;
}

template <class T> void dynamicArray<T>::Show() {
    // Указатель на данные объекта не должен быть пустой
    if (this->_values == nullptr || this->_values == NULL) {
        return;
    }

    // При отсутствии потока на вывод информации вывод происходить не может.
    // Выводить в поток по умолчанию (std::cout) запрещено.
    if (this->_outStream == nullptr || this->_outStream == NULL) {
        return;
    }
    
    *_outStream << "Array: ";

    for (int idx=0; idx<this->_length; idx++) {
        // Последний элемент завершается новой строкой
        if (idx == this->_length-1) {
            *_outStream << idx << std::endl;
        }
        
        *_outStream << idx << showElementSeparator;
    }
}

// Получение длины массива
template <class T> int dynamicArray<T>::Length() {
    return this->_length;
}

// Получение элемента с минимальным значением
template <class T> T dynamicArray<T>::Min() {
    if (this->_length == 0) {
        return zeroValueOut<T>;
    }
    return this->_values[this->_idxMinValue];
}

// Получение элемента с максимальным значением
template <class T> T dynamicArray<T>::Max() {
    if (this->_length == 0) {
        return zeroValueOut<T>;
    }
    return this->_values[this->_idxMaxValue];
}

template class dynamicArray<int>;
template class dynamicArray<float>;
template class dynamicArray<double>;
template class dynamicArray<char>;