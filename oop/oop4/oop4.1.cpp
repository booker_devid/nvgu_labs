#include <cstddef>
#include <iostream>
#include <array>

#include  "oop4.hpp"

// Размерность массива задана заранее
const int sizeArray = 4;

// Поиск минимального значения
template <typename T> T FindMin(void *args) {
    if (args == nullptr || args == NULL) {
        return zeroValueOut<T>;
    }

    T minValue;
    T *arr;
    arr = (T*)args;

    for (int idx = 0; idx < sizeArray; ++idx) {
        // Первое значение берется за минимальное
        if (idx == 0) {
            minValue = arr[idx];
            continue;
        }

        // Значение меньше предыдущего записывается в minValue
        if (arr[idx] < arr[idx-1]) {
            minValue = arr[idx];
        }
    }

    return minValue;
}


template int FindMin<int>(void*);
template float FindMin<float>(void*);
template double FindMin<double>(void*);
template char FindMin<char>(void*);