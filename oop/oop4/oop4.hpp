/*
    Задание: Дан радиус окружности, подсчитать длину окружности.
    Решение: библиотека из функций по нахождению длины окружности.
    
    Примеры и тесты в папке ../tests
*/

#ifndef OOP4_H
#define OOP4_H

#include <cstddef>
#include <ostream>

// Нулевое значение по умолчанию
template <typename T> static const T *zeroValueOut = "";
template <> inline const float zeroValueOut<float> = 0.0f;
template <> inline const float zeroValueOut<const float> = 0.0f;
template <> inline const double zeroValueOut<double> = 0.0f;
template <> inline const double zeroValueOut<const double> = 0.0f;
template <> inline const int zeroValueOut<int> = 0;
template <> inline const int zeroValueOut<const int> = 0;
template <> inline char zeroValueOut<char> = '0';
template <> inline char zeroValueOut<const char> = '0';

// ООП 4 лабораторная 1
template <typename T> T FindMin(void *args);

// ООП 4 лабораторная 2
template <class T>
class dynamicArray{
    public:
        dynamicArray(std::ostream *outStream);
        ~dynamicArray();
        
        // Для возможности по индексного обращения к элементам
        T &operator[](int i){return _values[i];};

        // Основной функционал
        void Add(T);
        void Sort();
        void Show(); 
        int Length();
        T Min();
        T Max();

    private:
        T *_values;

        // Текущий размер
        int _length;

        // Текущая вместительность
        int cap;

        int _idxMinValue;
        int _idxMaxValue;
        
        // Поток для вывода данных.
        // При отсутствие существующего потока вывод происходить не будет
        std::ostream *_outStream;
};

#endif