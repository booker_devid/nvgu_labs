#include "zero_replacer.h"

const int row_for_replace = 0;

// Подсчитываем количество 0
int ZeroCounter(DoubleArr* arr, int column, int row) {
    if (row == arr->len) {
        return 0;
    }

    // Рекурсия
    int count = ZeroCounter(arr, column, row+1);

    // Если столбца под номером N нет, то мы возвращаем 0
    if (arr->values[row].len < column) {
        return 0;
    }

    // Если значение равно 0, возвращаем 1
    if (arr->values[row].values[column] == 0) {
        count++;
    }

    return count;
}

// Заменяем первый нуль на количество повторений нуля в столбце. 0 в начале строки заменяется на 1.
void ZeroReplace(DoubleArr* arr) {
    if (arr->len == 0) {
        return;
    }

    // Проверяем все столбцы в строке row_for_replace
    for (int column = 0; column < arr->values[row_for_replace].len; column++) {
        int count = ZeroCounter(arr, column, 0);

        if (count > 0) {
            arr->values[0].values[column] = count;
        }
    }
}

