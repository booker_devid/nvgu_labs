#include <string.h>
#include <stdlib.h>

/*
    Примитивная реализация массива
*/

typedef struct {
    double *values;
    int len;
} Arr;

typedef struct {
    Arr *values;
    int len;
} DoubleArr;

Arr NewArr(double *values, int size);
DoubleArr NewDoubleArr();
void Append(DoubleArr* arr, Arr value);