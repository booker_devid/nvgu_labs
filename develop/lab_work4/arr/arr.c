#include "arr.h"

Arr NewArr(double *values, int size) {
    Arr v;
    v.values = (double*) malloc(size*sizeof(double));
    v.len = size;
    
    memcpy(v.values, values, sizeof(double)*size);
    return v;
}

DoubleArr NewDoubleArr() {
    DoubleArr v;
    v.values = NULL;
    v.len = 0;
    return v;
}

void Append(DoubleArr* arr, Arr value) {
    // Увеличиваем количество элементов
    arr->len++;

    // Увеличиваем размер массива
    Arr *buf = (Arr*) malloc(arr->len*sizeof(Arr));

    // Если были уже записанные значения, то требуется их перенести в следующие массивы
    if (arr->len > 1) {
        memcpy(buf, arr->values, sizeof(Arr)*arr->len-1);
        free(arr->values);
    }

    // Дополнение нового элемента
    buf[arr->len-1] = value;

    // Обозначение буфера в основе основного массива 
    arr->values = buf;
}