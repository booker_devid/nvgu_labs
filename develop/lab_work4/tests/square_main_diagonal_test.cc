/*
    Тесты для проверки корректности работоспособности
*/ 

// Тесты реализованы на С++ (gtest)
// Что бы подружить С и С++ используем extern
#include <cmath>
extern "C" {
    #include <math.h>
    #include <stdio.h>
    #include "../square_main_diagonal/square_main_diagonal.h"
}

#include <gtest/gtest.h>

// Проверка возведения в квадрат главной диагонали массива
TEST(TestSquaredMainDiagonal, Example1) {
    const int rows = 3;
    const int col = 3;
    //  Инициализация стартовых значений
    double values[rows][col] = {
        {2.0,2.0,2.0},
        {2.0,2.0,2.0},
        {2.0,2.0,2.0},
    };
    const double expect_value[rows][col] = {
        {4.0,2.0,2.0},
        {2.0,4.0,2.0},
        {2.0,2.0,4.0},
    };
    DoubleArr *arr = new DoubleArr;

    // Подготовка массива
    for (int idx = 0; idx < rows; idx++) {
        Arr row = NewArr((double *) values[idx], sizeof(values[idx])/sizeof(double *));
        Append(arr, row);
    }

    // Обработка массива
    SquaredMainDiagonal(arr);

    // Проверка измененных значений
    for (int idx_row = 0; idx_row < arr->len; idx_row++) {
        for (int idx_col = 0; idx_col < arr->values[idx_row].len; idx_col++) {
            ASSERT_EQ(expect_value[idx_row][idx_col], arr->values[idx_row].values[idx_col]);
        }
    }
}
