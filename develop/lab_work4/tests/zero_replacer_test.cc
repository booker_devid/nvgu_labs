/*
    Тесты для проверки корректности работоспособности
*/ 

// Тесты реализованы на С++ (gtest)
// Что бы подружить С и С++ используем extern
#include <cmath>
extern "C" {
    #include <math.h>
    #include <stdio.h>
    #include "../zero_replacer/zero_replacer.h"
}

#include <gtest/gtest.h>

// Проверка замены первого нуля на количество нулей в столбце 
TEST(TestZeroReplacer, Example1) {
    // Инициализация стартовых значений
    const int rows = 3;
    const int col = 3;

    // Изначальный массив
    double values[rows][col] = {
        {0,0,1},
        {0,2,2},
        {1,2,2},
    };

    // Итоговый результат
    const double expect_value[rows][col] = {
        {2,1,1},
        {0,2,2},
        {1,2,2},
    };
    DoubleArr *arr = new DoubleArr;

    // Подготовка массива
    for (int idx = 0; idx < rows; idx++) {
        Arr row = NewArr((double *) values[idx], sizeof(values[idx])/sizeof(double *));
        Append(arr, row);
    }

    // Обработка массива
    ZeroReplace(arr);

    // Проверка измененных значений
    for (int idx_row = 0; idx_row < arr->len; idx_row++) {
        for (int idx_col = 0; idx_col < arr->values[idx_row].len; idx_col++) {
            ASSERT_EQ(expect_value[idx_row][idx_col], arr->values[idx_row].values[idx_col]);
        }
    }
}


// Проверка замены первого нуля на количество нулей в столбце 
TEST(TestZeroReplacer, Example2) {
    //  Инициализация стартовых значений
    const int rows = 1;
    const int col = 3;
    
    // Изначальный массив
    double values[rows][col] = {
        {0,0,0},
    };

    // Итоговый результат
    const double expect_value[rows][col] = {
        {1,1,1},
    };
    DoubleArr *arr = new DoubleArr;

    // Подготовка массива
    for (int idx = 0; idx < rows; idx++) {
        Arr row = NewArr((double *) values[idx], sizeof(values[idx])/sizeof(double *));
        Append(arr, row);
    }

    // Обработка массива
    ZeroReplace(arr);

    // Проверка измененных значений
    for (int idx_row = 0; idx_row < arr->len; idx_row++) {
        for (int idx_col = 0; idx_col < arr->values[idx_row].len; idx_col++) {
            ASSERT_EQ(expect_value[idx_row][idx_col], arr->values[idx_row].values[idx_col]);
        }
    }
}

// Проверка замены первого нуля на количество нулей в столбце 
TEST(TestZeroReplacer, Example3) {
    //  Инициализация стартовых значений
    const int rows = 1;
    const int col = 3;
    
    // Изначальный массив
    double values[rows][col] = {
        {9,9,9},
    };

    // Итоговый результат
    const double expect_value[rows][col] = {
        {9,9,9},
    };
    DoubleArr *arr = new DoubleArr;

    // Подготовка массива
    for (int idx = 0; idx < rows; idx++) {
        Arr row = NewArr((double *) values[idx], sizeof(values[idx])/sizeof(double *));
        Append(arr, row);
    }

    // Обработка массива
    ZeroReplace(arr);

    // Проверка измененных значений
    for (int idx_row = 0; idx_row < arr->len; idx_row++) {
        for (int idx_col = 0; idx_col < arr->values[idx_row].len; idx_col++) {
            ASSERT_EQ(expect_value[idx_row][idx_col], arr->values[idx_row].values[idx_col]);
        }
    }
}