#include "square_main_diagonal.h"

void SquaredMainDiagonal(DoubleArr *arr) {
    for (int idx_row = 0; idx_row < arr->len; idx_row++) {
        for (int idx_col = 0; idx_col < arr->values[idx_row].len; idx_col++) {
            if (idx_row == idx_col) {
                arr->values[idx_row].values[idx_col] = pow( arr->values[idx_row].values[idx_col], 2); 
                break;
            }
        }
    }
}