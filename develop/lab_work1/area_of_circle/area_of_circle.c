#include  "area_of_circle.h"

// Число Пи
const double PI = 3.14;

// Вычисление диаметра
double Diameter(double radius) {
    return radius*radius;
}

// Вычисление длины окружности
double AreaCircle(double radius) {
    if (radius <= 0) {
        return 0;
    }

    return  PI * Diameter(radius);
}