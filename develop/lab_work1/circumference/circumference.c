#include  "circumference.h"

// Число Пи
const double PI = 3.14;

// Вычисление длины окружности
double Circumference(double radius) {
    if (radius <= 0) {
        return 0;
    }

    return  2 * PI * radius;
}