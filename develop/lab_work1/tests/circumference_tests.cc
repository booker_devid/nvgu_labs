/*
    Тесты для проверки корректности работоспособности поиска длины
*/ 

// Тесты реализованы на С++ (gtest)
// Что бы подружить С и С++ используем extern
extern "C" {
    #include <math.h>
    #include <stdio.h>
    #include "../circumference/circumference.h"
}

#include <gtest/gtest.h>

// Проверка длины окружности при значении радиуса: 1
TEST(TestCircumference, Radius1) {
    const double value = 1;
    const double expect_value = 6.28;

    // Получем значение и округляем до двух знаков после точки
    double circumference = ((int)(Circumference(value)*100)/100.0);
    ASSERT_EQ(expect_value, circumference);
}

// Проверка длины окружности при значении радиуса: 0.5
TEST(TestCircumference, Radius05) {
    const double value = 0.5f;
    const double expect_value = 3.14;

    // Получем значение и округляем до двух знаков после точки
    double circumference = ((int)(Circumference(value)*100)/100.0);
    ASSERT_EQ(expect_value, circumference);
}

// Проверка длины окружности при значении радиуса: 0
TEST(TestCircumference, Radius0) {
    const double value = 0.0f;
    const double expect_value = 0;

    // Получем значение и округляем до двух знаков после точки
    double circumference = ((int)(Circumference(value)*100)/100.0);
    ASSERT_EQ(expect_value, circumference);
}

// Проверка длины окружности при значении радиуса: -1
TEST(TestCircumference, RadiusS1) {
    const double value = -1;
    const double expect_value = 0;

    // Получем значение и округляем до двух знаков после точки
    double circumference = ((int)(Circumference(value)*100)/100.0);
    ASSERT_EQ(expect_value, circumference);
}