include(FetchContent)

FetchContent_Declare(
  googletest
  GIT_REPOSITORY https://github.com/google/googletest.git
  GIT_TAG        v1.14.0
)
FetchContent_MakeAvailable(googletest)
add_library(GTest::GTest INTERFACE IMPORTED)
target_link_libraries(GTest::GTest INTERFACE gtest_main)

# Перечень запускаемых тестов
add_executable(
  lab1_test 
  tests.cc # Точка входа 
  circumference_tests.cc 
  area_of_circle_tests.cc
  hypotenuse_tests.cc
)

target_link_libraries(
  lab1_test PRIVATE GTest::GTest 
  circumference 
  area_of_circle
  hypotenuse
)

# Объявляем тест
add_test(lab1_testing lab1_test)