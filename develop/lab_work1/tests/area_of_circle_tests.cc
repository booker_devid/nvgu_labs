/*
    Тесты для проверки корректности работоспособности поиска длины
*/ 

// Тесты реализованы на С++ (gtest)
// Что бы подружить С и С++ используем extern
extern "C" {
    #include <math.h>
    #include <stdio.h>
    #include "../area_of_circle/area_of_circle.h"
}

#include <gtest/gtest.h>

// Проверка площади круга при значении радиуса: 1
TEST(TestAreaCircle, Radius1) {
    const double value = 1;
    const double expect_value = 3.14;

    // Получем значение и округляем до двух знаков после точки
    double areaCircle = ((int)(AreaCircle(value)*100)/100.0);
    ASSERT_EQ(expect_value, areaCircle);
}

// Проверка площади круга при значении радиуса: 0.5
TEST(TestAreaCircle, Radius05) {
    const double value = 0.5;
    const double expect_value = 0.78;

    // Получем значение и округляем до двух знаков после точки
    double areaCircle = ((int)(AreaCircle(value)*100)/100.0);
    ASSERT_EQ(expect_value, areaCircle);
}

// Проверка площади круга при значении радиуса: 0
TEST(TestAreaCircle, Radius0) {
    const double value = 0.0;
    const double expect_value = 0;

    // Получем значение и округляем до двух знаков после точки
    double areaCircle = ((int)(AreaCircle(value)*100)/100.0);
    ASSERT_EQ(expect_value, areaCircle);
}

// Проверка площади круга при значении радиуса: -1
TEST(TestAreaCircle, RadiusS1) {
    const double value = -1;
    const double expect_value = 0;

    // Получем значение и округляем до двух знаков после точки
    double areaCircle = ((int)(AreaCircle(value)*100)/100.0);
    ASSERT_EQ(expect_value, areaCircle);
}