/*
    Тесты для проверки корректности работоспособности поиска длины
*/ 

// Тесты реализованы на С++ (gtest)
// Что бы подружить С и С++ используем extern
extern "C" {
    #include <math.h>
    #include <stdio.h>
    #include "../hypotenuse/hypotenuse.h"
}

#include <gtest/gtest.h>

// Проверка длины гипотенузы при значении катетов: 1, 5
TEST(TestHypotenuse, Legs1_5) {
    const double value_leg_f = 1;
    const double value_leg_s = 5;
    const double expect_value = 5.09;

    // Получем значение и округляем до двух знаков после точки
    double hypotenuse = ((int)(Hypotenuse(value_leg_f, value_leg_s)*100)/100.0);
    ASSERT_EQ(expect_value, hypotenuse);
}

// Проверка длины гипотенузы при значении катетов: 1, 3.5
TEST(TestHypotenuse, Legs0_35) {
    const double value_leg_f = 1;
    const double value_leg_s = 3.5;
    const double expect_value = 3.64;

    // Получем значение и округляем до двух знаков после точки
    double hypotenuse = ((int)(Hypotenuse(value_leg_f, value_leg_s)*100)/100.0);
    ASSERT_EQ(expect_value, hypotenuse);
}

// Проверка длины гипотенузы при значении катетов: 0, 1
TEST(TestHypotenuse, Legs0_1) {
    const double value_leg_f = 0;
    const double value_leg_s = 1;
    const double expect_value = 0;

    // Получем значение и округляем до двух знаков после точки
    double hypotenuse = ((int)(Hypotenuse(value_leg_f, value_leg_s)*100)/100.0);
    ASSERT_EQ(expect_value, hypotenuse);
}

// Проверка длины гипотенузы при значении катетов: 1.0, -1
TEST(TestHypotenuse, Legs10_S1) {
    const double value_leg_f = 1.0;
    const double value_leg_s = -1;
    const double expect_value = 0;

    // Получем значение и округляем до двух знаков после точки
    double hypotenuse = ((int)(Hypotenuse(value_leg_f, value_leg_s)*100)/100.0);
    ASSERT_EQ(expect_value, hypotenuse);
}