#include  "hypotenuse.h"
#include <math.h>

double ModLeg(double leg) {
    return leg*leg;
}

// Основной функционал поиска гипотенузы прямоугольного треугольника по длине катетов 
double Hypotenuse(double leg_f, double leg_s) {
    if (leg_f <= 0 || leg_s <= 0) {
        return 0;
    }

    // ( leg_first^2 + leg_second^2 ) / 2 
    return sqrt( ModLeg(leg_f) + ModLeg(leg_s) );
}