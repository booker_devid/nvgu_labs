#include "math.h"
#include <stdlib.h>
#include "check_three_and_abs.h"

bool CheckThreeNumAndModWithABS(double *n1, double *n2, double *n3) {
    bool result = *n1 > *n2 && *n2 > *n3;
    if (result) {
        // number ^ 3
        *n1 = pow(*n1,3);
        *n2 = pow(*n2,3);
        *n3 = pow(*n3,3);

    } else {
        // abs number 
        *n1 = (double) abs((int) *n1);
        *n2 = (double) abs((int) *n2);
        *n3 = (double) abs((int) *n3);

    }
    return result;
}