#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>

#include "max_search.h"

const int sizeResult = 3;

const char * tg_type_name = "tg";
const char * ctg_type_name = "ctg";
const char * ln_type_name = "ln";

// Позволяет сдвинуть плавающую точку на 6 символов
// и вернуть целочисленное значение для сравнения
double ConvertDouble(double val) {
    return val;
}

// Создаем объект с результатами подсчета
MathResults NewMathResults(double x) {
    double ctg = 0;
    if (tan(x) != 0) {
        ctg = ConvertDouble(1/tan(x));
    }

    const double tgResult = ConvertDouble(tan(x));
    const double ctgResult = ctg;
    const double lnResult = ConvertDouble(log(x));
    
    // Сохраняем результаты функций в heap
    double bufMathResults[] = {tgResult, ctgResult, lnResult};
    double * mathResults = (double *) malloc(sizeof(bufMathResults));
    memcpy(mathResults, bufMathResults, sizeof(bufMathResults));

    TypeResult maxValue;

    for (int idx = 0; idx < sizeResult; idx++) {
        // Первое значение является, по умолчанию, стартовым значением, 
        // для проверки максимального значения.
        if (idx == 0) {
            maxValue = (TypeResult) idx;
            continue;
        }

        // Поиск наибольшего значения между текущим и предшествующим значением.
        // Все значения преобразуются в целочисленные с точностью до 8 знаков 
        // после плавающей точки
        if (mathResults[idx] > mathResults[idx-1]) {
            maxValue = (TypeResult) idx;
        }
    }

    // Формируем результат
    MathResults results; 
    results.values = mathResults;
    results.maxValue = maxValue;
    return results;
}

double GetMaxValue(MathResults *results) {
    return GetValue(results, results->maxValue);
}

// Для получения значения
double GetValue(MathResults *results, int idx) {
    return results->values[idx];
}

// Возвращает копию наименования максимального значения
char* GetNameMaxValue(MathResults *results) {
    char * name;
    size_t nameSize;

    switch (results->maxValue) {
        case TG_TYPE:
            nameSize = strlen(tg_type_name);
            name = (char *) malloc(nameSize);
            memcpy(name, tg_type_name, nameSize);
            break;

        case CTG_TYPE:
            nameSize = strlen(ctg_type_name);
            name = (char *) malloc(nameSize);
            memcpy(name, ctg_type_name, nameSize);
            break;

        case LN_TYPE:
            nameSize = strlen(ln_type_name);
            name = (char *) malloc(nameSize);
            memcpy(name, ln_type_name, nameSize);
            break;
    }

    return name;
}

void Clear(MathResults *results) {
    free(results->values);
}