#define MAX_LENGTH_NUMBER 20

/*
    Результат сохраняет список значений и определяет какое из них является
    Последовательность значений в values определяется индексом
     0 - tg(n)
     1 - ctg(n)
     2 - ln(n)

    Size указывает на количество результатов
*/

// Тип результата
typedef enum { TG_TYPE, CTG_TYPE, LN_TYPE } TypeResult;

typedef struct  {
    double *values;
    TypeResult maxValue;
} MathResults;

// Основной функционал
MathResults NewMathResults(double x);
double GetValue(MathResults *results, int idx);
double GetMaxValue(MathResults *results);
char* GetNameMaxValue(MathResults *results);
void PrintResult();
void Clear(MathResults *results);


// Дополнительный функционал
double ConvertDouble(double val);