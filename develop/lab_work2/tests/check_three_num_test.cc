/*
    Тесты для проверки корректности работоспособности
*/ 

// Тесты реализованы на С++ (gtest)
// Что бы подружить С и С++ используем extern
extern "C" {
    #include <math.h>
    #include <stdio.h>
    #include "../check_three_num/check_three_num.h"
}

#include <gtest/gtest.h>

// Проверка трех чисел на условие v1 < v2 < v3 : 1 2 3 | true
TEST(TestCheckTreeNumbers, Version_1) {
    const double value1 = 1;
    const double value2 = 2;
    const double value3 = 3;
    const bool expect_value = true;

    ASSERT_EQ(expect_value, CheckThreeNumber(value1, value2, value3));
}

// Проверка трех чисел на условие v1 < v2 < v3 : -3 -2 -1 | true
TEST(TestCheckTreeNumbers, Version_2) {
    const double value1 = -3;
    const double value2 = -2;
    const double value3 = -1;
    const bool expect_value = true;

    ASSERT_EQ(expect_value, CheckThreeNumber(value1, value2, value3));
}

// Проверка трех чисел на условие v1 < v2 < v3 : 1 0 3 | false
TEST(TestCheckTreeNumbers, Version_3) {
    const double value1 = 1;
    const double value2 = 0;
    const double value3 = 3;
    const bool expect_value = false;

    ASSERT_EQ(expect_value, CheckThreeNumber(value1, value2, value3));
}

// Проверка трех чисел на условие v1 < v2 < v3 : -1 -2 -3 | false
TEST(TestCheckTreeNumbers, Version_4) {
    const double value1 = -1;
    const double value2 = -2;
    const double value3 = -3;
    const bool expect_value = false;

    ASSERT_EQ(expect_value, CheckThreeNumber(value1, value2, value3));
}

// Проверка трех чисел на условие v1 < v2 < v3 : 0 0 0 | false
TEST(TestCheckTreeNumbers, Version_5) {
    const double value1 = 0;
    const double value2 = 0;
    const double value3 = 0;
    const bool expect_value = false;

    ASSERT_EQ(expect_value, CheckThreeNumber(value1, value2, value3));
}

// Проверка трех чисел на условие v1 < v2 < v3 : 1 2 1 | false
TEST(TestCheckTreeNumbers, Version_6) {
    const double value1 = 1;
    const double value2 = 2;
    const double value3 = 1;
    const bool expect_value = false;

    ASSERT_EQ(expect_value, CheckThreeNumber(value1, value2, value3));
}