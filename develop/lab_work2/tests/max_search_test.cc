/*
    Тесты для проверки корректности работоспособности
*/ 

// Тесты реализованы на С++ (gtest)
// Что бы подружить С и С++ используем extern
extern "C" {
    #include <math.h>
    #include <stdio.h>
    #include "../max_search/max_search.h"
}

#include <gtest/gtest.h>

const char * formatResult = "RESULT\n\tvalues: %.5lf, %.5lf, %.5lf\n\tindex max value: %d\n\tname max value: %s";

// Функция для вывода результата содержимого в result
void PrintResult(MathResults *result) {
    printf(formatResult,
        GetValue(result,0),
        GetValue(result,1),
        GetValue(result,2),
        GetMaxValue(result),
        GetNameMaxValue(result)
    );
}

// Проверка максимального значение. Макс Tg
TEST(TestMaxSearch, MaxSearch_Tan) {
    const double value = 1.30;
    const double expect_value = ConvertDouble(tan(value));
    const char * expect_name = "tg";


    MathResults result = NewMathResults(value);
    PrintResult(&result);

    ASSERT_EQ(expect_value, GetMaxValue(&result));
    EXPECT_STREQ(expect_name, GetNameMaxValue(&result));
    
    // Чистим память
    Clear(&result);
}

// Проверка максимального значение. Макс Ctg
TEST(TestMaxSearch, MaxSearch_Ctan) {
    const double value = -1.30;
    const double expect_value = ConvertDouble(1/tan(value));
    const char * expect_name = "ctg";

    MathResults result = NewMathResults(value);
    PrintResult(&result);

    ASSERT_EQ(expect_value, GetMaxValue(&result));
    EXPECT_STREQ(expect_name, GetNameMaxValue(&result));

    // Чистим память
    Clear(&result);
}


// Проверка максимального значение. Макс Ln
TEST(TestMaxSearch, MaxSearch_Ln) {
    const double value = 230;
    const double expect_value = ConvertDouble(log(value));
    const char * expect_name = "ln";

    MathResults result = NewMathResults(value);
    PrintResult(&result);

    ASSERT_EQ(expect_value, GetMaxValue(&result));
    EXPECT_STREQ(expect_name, GetNameMaxValue(&result));

    // Чистим память
    Clear(&result);
}