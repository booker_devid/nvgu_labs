/*
    Тесты для проверки корректности работоспособности
*/ 

// Тесты реализованы на С++ (gtest)
// Что бы подружить С и С++ используем extern
#include <cmath>
extern "C" {
    #include <math.h>
    #include <stdio.h>
    #include "../check_three_and_abs/check_three_and_abs.h"
}

#include <gtest/gtest.h>

// Проверка трех чисел на условие v1 > v2 > v3 : 3 2 1 | true
TEST(TestCheckThreeNumAndModWithABS, Version_1) {
    double value1 = 3;
    double value2 = 2;
    double value3 = 1;
    const bool expect_result = true;

    const double expect_value1 = pow(value1,3);
    const double expect_value2 = pow(value2,3);
    const double expect_value3 = pow(value3,3);

    ASSERT_EQ(expect_result, CheckThreeNumAndModWithABS(&value1, &value2, &value3));
    ASSERT_EQ(expect_value1, value1);
    ASSERT_EQ(expect_value2, value2);
    ASSERT_EQ(expect_value3, value3);
}

// Проверка трех чисел на условие v1 > v2 > v3 : -1 -2 -3 | true
TEST(TestCheckThreeNumAndModWithABS, Version_2) {
    double value1 = -1;
    double value2 = -2;
    double value3 = -3;
    const bool expect_result = true;

    const double expect_value1 = pow(value1,3);
    const double expect_value2 = pow(value2,3);
    const double expect_value3 = pow(value3,3);

    ASSERT_EQ(expect_result, CheckThreeNumAndModWithABS(&value1, &value2, &value3));
    ASSERT_EQ(expect_value1, value1);
    ASSERT_EQ(expect_value2, value2);
    ASSERT_EQ(expect_value3, value3);
}

// Проверка трех чисел на условие v1 > v2 > v3 : 1 2 3 | false
TEST(TestCheckThreeNumAndModWithABS, Version_3) {
    double value1 = 1;
    double value2 = 2;
    double value3 = 3;
    const bool expect_result = false;

    const double expect_value1 = (double) abs((int) value1);
    const double expect_value2 = (double) abs((int) value2);
    const double expect_value3 = (double) abs((int) value3);

    ASSERT_EQ(expect_result, CheckThreeNumAndModWithABS(&value1, &value2, &value3));
    ASSERT_EQ(expect_value1, value1);
    ASSERT_EQ(expect_value2, value2);
    ASSERT_EQ(expect_value3, value3);
}

// Проверка трех чисел на условие v1 > v2 > v3 : 0 0 0 | false
TEST(TestCheckThreeNumAndModWithABS, Version_4) {
    double value1 = 0;
    double value2 = 0;
    double value3 = 0;
    const bool expect_result = false;

    const double expect_value1 = (double) abs((int) value1);
    const double expect_value2 = (double) abs((int) value2);
    const double expect_value3 = (double) abs((int) value3);

    ASSERT_EQ(expect_result, CheckThreeNumAndModWithABS(&value1, &value2, &value3));
    ASSERT_EQ(expect_value1, value1);
    ASSERT_EQ(expect_value2, value2);
    ASSERT_EQ(expect_value3, value3);
}

// Проверка трех чисел на условие v1 > v2 > v3 : NaN INFINITY NaN | false
TEST(TestCheckThreeNumAndModWithABS, Version_5) {
    double value1 = NAN;
    double value2 = INFINITY;
    double value3 = NAN;
    const bool expect_result = false;

    const double expect_value1 = (double) abs((int) value1);
    const double expect_value2 = (double) abs((int) value2);
    const double expect_value3 = (double) abs((int) value3);

    ASSERT_EQ(expect_result, CheckThreeNumAndModWithABS(&value1, &value2, &value3));
    ASSERT_EQ(expect_value1, value1);
    ASSERT_EQ(expect_value2, value2);
    ASSERT_EQ(expect_value3, value3);
}