#include "check_three_num.h"

bool CheckThreeNumber(double n1, double n2, double n3) {
    return n1 < n2 && n2 < n3;
}