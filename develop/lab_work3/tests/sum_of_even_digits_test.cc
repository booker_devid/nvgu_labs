/*
    Тесты для проверки корректности работоспособности
*/ 

// Тесты реализованы на С++ (gtest)
// Что бы подружить С и С++ используем extern
#include <cmath>
extern "C" {
    #include <math.h>
    #include <stdio.h>
    #include "../sum_of_even_digits/sum_of_even_digits.h"
}

#include <gtest/gtest.h>

// Проверка получения суммы четных цифр в числе 0
TEST(TestFindSumOfEvenDigits, EvenCountFrom0) {
    int value = 0;
    const int expect_result = 1;

    ASSERT_EQ(expect_result, FindSumOfEvenDigits(value));
}

// Проверка получения суммы четных цифр в числе -0
TEST(TestFindSumOfEvenDigits, EvenCountFromSub0) {
    int value = -0;
    const int expect_result = 1;

    ASSERT_EQ(expect_result, FindSumOfEvenDigits(value));
}

// Проверка получения суммы четных цифр в числе 1
TEST(TestFindSumOfEvenDigits, EvenCountFrom1) {
    int value = 1;
    const int expect_result = 0;

    ASSERT_EQ(expect_result, FindSumOfEvenDigits(value));
}

// Проверка получения суммы четных цифр в числе 2
TEST(TestFindSumOfEvenDigits, EvenCountFrom2) {
    int value = 2;
    const int expect_result = 1;

    ASSERT_EQ(expect_result, FindSumOfEvenDigits(value));
}

// Проверка получения суммы четных цифр в числе 22
TEST(TestFindSumOfEvenDigits, EvenCountFrom22) {
    int value = 22;
    const int expect_result = 2;

    ASSERT_EQ(expect_result, FindSumOfEvenDigits(value));
}

// Проверка получения суммы четных цифр в числе 122
TEST(TestFindSumOfEvenDigits, EvenCountFrom122) {
    int value = 122;
    const int expect_result = 2;

    ASSERT_EQ(expect_result, FindSumOfEvenDigits(value));
}

// Проверка получения суммы четных цифр в числе 1210
TEST(TestFindSumOfEvenDigits, EvenCountFrom1210) {
    int value = 1210;
    const int expect_result = 2;

    ASSERT_EQ(expect_result, FindSumOfEvenDigits(value));
}

// Проверка получения суммы четных цифр в числе 222001
TEST(TestFindSumOfEvenDigits, EvenCountFrom222001) {
    int value = 222001;
    const int expect_result = 5;

    ASSERT_EQ(expect_result, FindSumOfEvenDigits(value));
}

// Проверка получения суммы четных цифр в числе -222001
TEST(TestFindSumOfEvenDigits, EvenCountFromSub222001) {
    int value = -222001;
    const int expect_result = 5;

    ASSERT_EQ(expect_result, FindSumOfEvenDigits(value));
}

// Проверка получения суммы четных цифр в числе 1357
TEST(TestFindSumOfEvenDigits, EvenCountFrom1357) {
    int value = 1357;
    const int expect_result = 0;

    ASSERT_EQ(expect_result, FindSumOfEvenDigits(value));
}