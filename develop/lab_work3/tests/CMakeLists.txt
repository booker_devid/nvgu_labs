include(FetchContent)

FetchContent_Declare(
  googletest
  GIT_REPOSITORY https://github.com/google/googletest.git
  GIT_TAG        v1.14.0
)
FetchContent_MakeAvailable(googletest)
add_library(GTest::GTest INTERFACE IMPORTED)
target_link_libraries(GTest::GTest INTERFACE gtest_main)

# Перечень запускаемых тестов
add_executable(
  lab3_test 
  tests.cc # Точка входа 
  divisible_by_three_test.cc
  sum_of_even_digits_test.cc
)

target_link_libraries(
  lab3_test PRIVATE GTest::GTest 
  divisible_by_three
  sum_of_even_digits
)

# Объявляем тест
add_test(lab3_testing lab3_test)