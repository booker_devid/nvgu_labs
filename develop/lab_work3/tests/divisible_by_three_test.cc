/*
    Тесты для проверки корректности работоспособности
*/ 

// Тесты реализованы на С++ (gtest)
// Что бы подружить С и С++ используем extern
#include <cmath>
extern "C" {
    #include <math.h>
    #include <stdio.h>
    #include "../divisible_by_three/divisible_by_three.h"
}

#include <gtest/gtest.h>

// Проверка получения суммы чисел делимых на 3 без остатка от 1 до 0
TEST(TestFindDivisibleByThree, From1To0) {
    int value = 0;
    const int expect_result = 0;

    ASSERT_EQ(expect_result, FindDivisibleByThree(value));
}

// Проверка получения суммы чисел делимых на 3 без остатка от 0 до 3
TEST(TestFindDivisibleByThree, From1To3) {
    int value = 3;
    const int expect_result = 3;

    ASSERT_EQ(expect_result, FindDivisibleByThree(value));
}

// Проверка получения суммы чисел делимых на 3 без остатка от 0 до -3
TEST(TestFindDivisibleByThree, From1ToSub3) {
    int value = -3;
    const int expect_result = -3;

    ASSERT_EQ(expect_result, FindDivisibleByThree(value));
}

// Проверка получения суммы чисел делимых на 3 без остатка от 0 до 1
TEST(TestFindDivisibleByThree, From1ToSub1) {
    int value = 1;
    const int expect_result = 0;

    ASSERT_EQ(expect_result, FindDivisibleByThree(value));
}

// Проверка получения суммы чисел делимых на 3 без остатка от 0 до 10
TEST(TestFindDivisibleByThree, From1To10) {
    int value = 10;
    const int expect_result = 18;

    ASSERT_EQ(expect_result, FindDivisibleByThree(value));
}

