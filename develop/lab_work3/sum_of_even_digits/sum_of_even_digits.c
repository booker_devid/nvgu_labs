#include "sum_of_even_digits.h"

int FindSumOfEvenDigits(int numer) {
    int steps = abs(numer);
    int evenCount = 0;

    while (steps != 0) {
        int num = steps%10;

        if (num%2 == 0) {
            evenCount++;
        }

        steps = steps/10;
    }

    // 0 четная цифра не попадает в цикл, но учитывается в результате, если в качестве числа была передана цифра 0
    if (numer == 0) {
        evenCount++;
    }
    
    return evenCount;
}