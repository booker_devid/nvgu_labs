// Дано число n. В интервале от 1 до n сложить все числа, которые делятся на 3 без остатка.
#include <stdlib.h>

int FindDivisibleByThree(int interval);