#include "divisible_by_three.h"

int FindDivisibleByThree(int interval) {
    int step = 1;

    // В случае если n < 0, то требуется идти на убывание
    if (interval < 0) {
        step = -1;
    } 

    int result = 0;
    // В случае если движение идет в отрицательную сторону, то значение индекса итератора
    // и интервала берутся в модуле. 1 to -3 (iterator --) == |1| to |-3| (iterator ++)
    for (int idx = 1; abs(idx) <= abs(interval); idx += step) {
        if (idx%3 == 0) {
            result += idx;
        }
    } 
    
    return result;
}