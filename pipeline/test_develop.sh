#!/bin/bash

for f in develop/*;  do 
    if [ -d "${f}"  -a ! -h "${f}" ];  
    then  
        cd "${f}";  
        
        echo "Run build {`pwd`}"; 
        make -f MakeFile build
        make -f MakeFile test
       
        cd ../..; 
    fi;  
done;

