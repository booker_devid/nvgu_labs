# Лабораторные работы НВГУ

Предметы и каталоги лабораторных работ:
| Предмет | Каталог |
|---|---|
| Программирование | [develop](./develop/README.md) | 
| Объектно ориентированное программирование | [OOP](./oop/README.md) |
| Основы искусственного интеллекта | [ai_since](./ai_since/README.md) |

## Дополнительные ресурсы:
-   [Установка gtest](https://cpp-python-nsu.inp.nsk.su/textbook/sec2/ch7)
